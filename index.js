const express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var { startMqttService, sendAlert } = require("./src/mqtt");
var { startPushService, sendTestPush } = require("./src/fcm-push");

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.post("/alert", async function (req, res) {
  console.log(req.body);
  await sendAlert(req.body.text);
  res.send("Alert sent?");
});

app.post("/send-push", async function (req, res) {
  console.log(req.body);

  try {
    await sendTestPush(req.body.text);
    res.status(200).send("Successfully sent message");
  } catch (e) {
    res.status(400).send("Error");
  }
});

app.get("/", function (req, res) {
  res.send("Home");
});

app.listen(3001);

startPushService();
startMqttService();
