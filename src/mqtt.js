const { iot, mqtt5 } = require("aws-iot-device-sdk-v2");
const { toUtf8 } = require("@aws-sdk/util-utf8-browser");

const builder =
  iot.AwsIotMqtt5ClientConfigBuilder.newDirectMqttBuilderWithMtlsFromPath(
    "a1l7hjkn8mo7us-ats.iot.eu-west-1.amazonaws.com",
    "./cert/7d5cc3d35a4f9da9a98b03dd4b3bb765304296e2fab3cc4e1d219d38f5c38027-certificate.pem.crt",
    "./cert/7d5cc3d35a4f9da9a98b03dd4b3bb765304296e2fab3cc4e1d219d38f5c38027-private.pem.key"
  );

builder.withConnectProperties({
  clientId: "fdsfdsfdsfsdfsdfds",
  keepAliveIntervalSeconds: 1200,
});

const client = new mqtt5.Mqtt5Client(builder.build());

client.on("error", (error) => {
  console.log("Error event: " + error.toString());
});

client.on("messageReceived", (eventData) => {
  console.log(
    "Message Received event: " + JSON.stringify(eventData.message, null, 2)
  );
  if (eventData.message.payload) {
    console.log(
      "  with payload: ",
      JSON.parse(toUtf8(new Uint8Array(eventData.message.payload)))
    );
  }
});

client.on("attemptingConnect", (eventData) => {
  console.log("Attempting Connect event");
});

client.on("connectionSuccess", (eventData) => {
  console.log("Connection Success event");
  console.log("Connack: " + JSON.stringify(eventData.connack));
  console.log("Settings: " + JSON.stringify(eventData.settings));
  subscribeToTopics();
});

client.on("connectionFailure", (eventData) => {
  console.log("Connection failure event: " + eventData.error.toString());
  if (eventData.connack) {
    console.log("Connack: " + JSON.stringify(eventData.connack));
  }
});

client.on("disconnection", (eventData) => {
  console.log("Disconnection event: " + eventData.error.toString());
  if (eventData.disconnect !== undefined) {
    console.log("Disconnect packet: " + JSON.stringify(eventData.disconnect));
  }
});

client.on("stopped", (eventData) => {
  console.log("Stopped event");
});

const subscribeToTopics = async () => {
  const suback = await client.subscribe({
    subscriptions: [
      { qos: mqtt5.QoS.AtLeastOnce, topicFilter: "hello/world/qos1" },
      { qos: mqtt5.QoS.AtLeastOnce, topicFilter: "hello/world/qos0" },
    ],
  });
  console.log("Suback result: " + JSON.stringify(suback));
};

const startMqttService = () => {
  client.start();
};

const sendAlert = async (text) => {
  await client.publish({
    payload: text,
    qos: mqtt5.QoS.AtLeastOnce,
    topicName: "stl/alarm/test",
  });
};

module.exports = {
  startMqttService,
  sendAlert,
};
