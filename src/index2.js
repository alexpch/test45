const express = require("express");
const {
  middleware: authMiddleware,
  router: authRouter,
} = require("#auth-service");
const {
  middleware: deviceMiddleware,
  router: deviceRouter,
} = require("#device-service");
const {
  middleware: iotMiddleware,
  router: iotRouter,
} = require("#iot-service");
const authContracts = require("#contracts/auth.js");
const { mqtt, iot } = require("aws-iot-device-sdk-v2");

const config_builder =
  iot.AwsIotMqttConnectionConfigBuilder.new_mtls_builder_from_path(
    "./cert/7d5cc3d35a4f9da9a98b03dd4b3bb765304296e2fab3cc4e1d219d38f5c38027-certificate.pem.crt",
    "./cert/7d5cc3d35a4f9da9a98b03dd4b3bb765304296e2fab3cc4e1d219d38f5c38027-private.pem.key"
  );

config_builder.with_certificate_authority_from_path(
  undefined,
  "./cert/AmazonRootCA1.pem"
);
config_builder.with_clean_session(false);
config_builder.with_client_id("test-" + Math.floor(Math.random() * 100000000));
config_builder.with_endpoint("a1l7hjkn8mo7us-ats.iot.eu-west-1.amazonaws.com");
const config = config_builder.build();

const client = new mqtt.MqttClient();
const connection = client.new_connection(config);

// client.on("error", (error) => {
//   console.log("Error event: " + error.toString());
// });

// client.on("messageReceived", (eventData) => {
//   console.log("Message Received event: " + JSON.stringify(eventData.message));
//   if (eventData.message.payload) {
//     console.log(
//       "  with payload: " + toUtf8(new Uint8Array(eventData.message.payload))
//     );
//   }
// });

// client.on("attemptingConnect", (eventData) => {
//   console.log("Attempting Connect event");
// });

// client.on("connectionSuccess", (eventData) => {
//   console.log("Connection Success event");
//   console.log("Connack: " + JSON.stringify(eventData.connack));
//   console.log("Settings: " + JSON.stringify(eventData.settings));
// });

// client.on("connectionFailure", (eventData) => {
//   console.log("Connection failure event: " + eventData.error.toString());
//   if (eventData.connack) {
//     console.log("Connack: " + JSON.stringify(eventData.connack));
//   }
// });

// client.on("disconnection", (eventData) => {
//   console.log("Disconnection event: " + eventData.error.toString());
//   if (eventData.disconnect !== undefined) {
//     console.log("Disconnect packet: " + JSON.stringify(eventData.disconnect));
//   }
// });

// client.on("stopped", (eventData) => {
//   console.log("Stopped event");
// });

connection
  .connect()
  .then((e) => console.log(e))
  .catch((e) => console.error(e));

const app = express();

app.use(authMiddleware);
app.use(deviceMiddleware);
app.use(iotMiddleware);

app.get("/", function (req, res) {
  res.send("Home");
});

authContracts.authFindUser().then((res) => {
  console.log(res);
});

authContracts.getRestData().then((res) => {
  console.log(res);
});

app.use("/auth", authRouter);
app.use("/device", deviceRouter);
app.use("/iot", iotRouter);

app.listen(3000);
