const { initializeApp, applicationDefault } = require("firebase-admin/app");
const { getMessaging } = require("firebase-admin/messaging");
const {
  FIREBASE_ANDROID_PUSH_TOKEN,
  FIREBASE_IOS_PUSH_TOKEN,
} = require("./config");

var admin = require("firebase-admin");

var serviceAccount = require("../cert/settlementsapp-dev-firebase-adminsdk-5619j-5941c5df5d.json");

// process.env.GOOGLE_APPLICATION_CREDENTIALS;
const startPushService = () => {
  // initializeApp({
  //   credential: applicationDefault(),
  //   databaseURL: "https://testmaxsound.firebaseio.com",
  //   projectId: "testmaxsound",
  // });
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });
};

const sendTestPush = async (text) => {
  const messageText = text;

  const tokens = [FIREBASE_ANDROID_PUSH_TOKEN, FIREBASE_IOS_PUSH_TOKEN];

  for (const i in tokens) {
    const message1 = {
      token: tokens[i],
      data: {
        title: "Notif",
        Nick: "Mario",
        body: messageText,
        Room: "PortugalVSDenmark",
      },
      android: {
        notification: {
          channelId: "notify-channel",
        },
        priority: "high",
      },
    };

    const message = {
      token: tokens[i],
      notification: {
        title: "Notif",
        body: messageText,
      },
      android: {
        notification: {
          channelId: "notify-channel",
        },
        priority: "high",
      },
      apns: {
        payload: {
          aps: {
            criticalSound: {
              critical: true,
              name: "default",
              volume: 1.0,
            },
          },
        },
      },
    };

    try {
      const response = await getMessaging().send(message);
      console.log("Successfully sent message:", response);
    } catch (e) {
      console.log("Error sending message:", e);
      throw new Error(e);
    }
  }
};

module.exports = {
  startPushService,
  sendTestPush,
};
